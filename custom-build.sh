#!/bin/sh

# This is a custom build script used when installing on CHICKEN 5.
# The script attempts to automatically discover the compiler flags
# needed to link with SDL2, unless the flags have already been
# specified via SDL2_CFLAGS and SDL2_LDFLAGS environment variables.
#
# This build script also checks SDL2_TTF_CFLAGS and
# SDL2_TTF_LDFLAGS, which are used in addition to the SDL2 flags.

if test -z "$CHICKEN_CSC"; then
    echo "Don't run this script directly. Instead run chicken-install."
    exit 1
fi


# Arguments passed from the build system. Will be passed to csc,
# probably with additional flags appended.
CSC_FLAGS="$@"


# If sdl2-config is available, and the environment variables are unset
# or empty, automatically discover the compiler and linker flags.
if command -v sdl2-config >/dev/null 2>&1; then
    : ${SDL2_CFLAGS:=$(sdl2-config --cflags)}
    : ${SDL2_LDFLAGS:=$(sdl2-config --libs)}
fi


# If using macOS, and the SDL2 and SDL2_ttf frameworks are
# installed, and the linker flags are still unset or empty, then use
# the frameworks.
if test "$(uname -s)" = "Darwin"; then
    if test \( -e "/Library/Frameworks/SDL2.framework" -o \
            -e "/System/Library/Frameworks/SDL2.framework" \) -a \
            \( -e "/Library/Frameworks/SDL2_ttf.framework" -o \
            -e "/System/Library/Frameworks/SDL2_ttf.framework" \)
    then
        : ${SDL2_LDFLAGS:="-framework SDL2"}
        : ${SDL2_TTF_LDFLAGS:="-framework SDL2_ttf"}
    fi
fi


# If SDL2_TTF_LDFLAGS is still unset or empty, use the default.
: ${SDL2_TTF_LDFLAGS:="-lSDL2_ttf"}


# Prefix each compiler flag with -C and each linker flag with -L so
# that csc will pass them through as flags to the compiler/linker.
for flag in $SDL2_CFLAGS; do
    CSC_FLAGS="$CSC_FLAGS -C $flag"
done
for flag in $SDL2_LDFLAGS; do
    CSC_FLAGS="$CSC_FLAGS -L $flag"
done
for flag in $SDL2_TTF_CFLAGS; do
    CSC_FLAGS="$CSC_FLAGS -C $flag"
done
for flag in $SDL2_TTF_LDFLAGS; do
    CSC_FLAGS="$CSC_FLAGS -L $flag"
done


# If on macOS, and using the frameworks, define a feature identifier
# so that the correct header file can be included.
if test "$(uname -s)" = "Darwin"; then
    case $CSC_FLAGS in
        *\ -framework\ *)
            CSC_FLAGS="$CSC_FLAGS -feature sdl2-ttf-use-mac-framework" ;;
    esac
fi


# Print the compiler command, to help debugging.
if test -n "$SDL2_VERBOSE_INSTALL"; then
    echo "      # $CHICKEN_CSC $CSC_FLAGS"
fi


# Run the compiler.
$CHICKEN_CSC $CSC_FLAGS
EXIT_STATUS=$?


# Print a help message if compilation failed and flags are empty.
if test "$EXIT_STATUS" -eq 0 -a -z "$SDL2_CFLAGS$SDL2_LDFLAGS"; then
    cat <<EOF

Note: Try setting the SDL2_CFLAGS, SDL2_LDFLAGS, SDL2_TTF_CFLAGS,
and/or SDL2_TTF_LDFLAGS environment variables to specify compiler and
linker flags. Example:

    export SDL2_CFLAGS="-I/usr/local/include/SDL2"
    export SDL2_LDFLAGS="-L/usr/local/lib -lSDL2"
    export SDL2_TTF_LDFLAGS="-lSDL2_ttf"
    chicken-install sdl2
EOF
fi


# Exit this script with the same status code as the compiler, so that
# the build process will stop if compilation failed.
exit $EXIT_STATUS
