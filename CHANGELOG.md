
This document describes the changes in each version of chicken-sdl2-ttf.

This library follows "[semantic versioning](http://semver.org)".
Until version 1.0.0 is released, the API is not guaranteed to be "stable".
That means the maintainer reserves the right to change the API if needed,
possibly in ways that break backwards compatibility with previous versions.
**Large backward-incompatible changes are unlikely**,
but there may be small tweaks and fixes to the API if problems are discovered.

After version 1.0.0 is released, the API is guaranteed to remain stable (no backward-incompatible changes)
until the next new major version (e.g. going from version 1.x to 2.0.0, or 2.x to 3.0.0).


# 0.2.0 (2019-02-13)

- Ported the egg to be compatible with both CHICKEN 4 and CHICKEN 5.

- Improved the way the installer handles compiler and linker flags.
  It will now attempt to automatically discover the correct compiler and linker flags,
  using the `sdl2-config` command.
  In special cases, set the `SDL2_CFLAGS`, `SDL2_LDFLAGS`,
  `SDL2_TTF_CFLAGS`, and/or `SDL2_TTF_LDFLAGS` environment variables.

- Added support for the SDL2 and SDL2_ttf frameworks on macOS.
  The installer will automatically use the frameworks
  if `sdl2-config` is not available and the frameworks are installed
  in either the `/Library/Frameworks` or `/System/Library/Frameworks` directories.
  In special cases, set the environment variables described above.


# 0.1.0 (2015-12-25)

Initial release. The following procedures were included:

## General

```
init!                         was-init?                     quit!
compiled-version              current-version
```

## Font

```
open-font                     open-font*
open-font-rw                  open-font-rw*
font?
close-font!

font-style                    font-style-set!
font-outline                  font-outline-set!
font-hinting                  font-hinting-set!
font-kerning                  font-kerning-set!

font-height
font-ascent
font-descent
font-line-skip

font-faces
font-face-fixed-width?
font-face-family-name
font-face-style-name

glyph-provided
glyph-metrics
```

## Rendering

```
size-text
render-text-solid             render-text-solid*
render-text-shaded            render-text-shaded*
render-text-blended           render-text-blended*

size-utf8
render-utf8-solid             render-utf8-solid*
render-utf8-shaded            render-utf8-shaded*
render-utf8-blended           render-utf8-blended*

byte-swapped-unicode-set!
size-unicode
render-unicode-solid          render-unicode-solid*
render-unicode-shaded         render-unicode-shaded*
render-unicode-blended        render-unicode-blended*

render-glyph-solid            render-glyph-solid*
render-glyph-shaded           render-glyph-shaded*
render-glyph-blended          render-glyph-blended*
```

