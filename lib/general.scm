;;
;; chicken-sdl2-ttf: CHICKEN Scheme bindings to SDL_ttf 2
;;
;; Copyright © 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export init!
        was-init?
        quit!

        current-version
        compiled-version)


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INIT / QUIT

(define-function-binding TTF_Init
  return: (int zero-on-success))

(define-function-binding TTF_WasInit
  return: (bool was-init?))

(define-function-binding TTF_Quit)


(: init! (-> void))
(define (init!)
  (let ((ret-code (TTF_Init)))
    (unless (zero? ret-code)
      (abort (sdl-failure "TTF_Init" ret-code)))))

(: init! (-> boolean))
(define (was-init?)
  (TTF_WasInit))

(: quit! (-> void))
(define (quit!)
  (TTF_Quit))



;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VERSION

(: current-version (-> (list fixnum fixnum fixnum)))
(define (current-version)
  (define foreign-getter
    (foreign-lambda*
     void ((Uint8* majorOut) (Uint8* minorOut) (Uint8* patchOut))
     "const SDL_version* v = TTF_Linked_Version();"
     "*majorOut = v->major;"
     "*minorOut = v->minor;"
     "*patchOut = v->patch;"))
  (with-temp-mem ((major-out (%allocate-Uint8))
                  (minor-out (%allocate-Uint8))
                  (patch-out (%allocate-Uint8)))
    (foreign-getter major-out minor-out patch-out)
    (list (pointer-u8-ref major-out)
          (pointer-u8-ref minor-out)
          (pointer-u8-ref patch-out))))


(: compiled-version (-> (list fixnum fixnum fixnum)))
(define (compiled-version)
  (define foreign-getter
    (foreign-lambda*
     void ((Uint8* majorOut) (Uint8* minorOut) (Uint8* patchOut))
     "*majorOut = TTF_MAJOR_VERSION;"
     "*minorOut = TTF_MINOR_VERSION;"
     "*patchOut = TTF_PATCHLEVEL;"))
  (with-temp-mem ((major-out (%allocate-Uint8))
                  (minor-out (%allocate-Uint8))
                  (patch-out (%allocate-Uint8)))
    (foreign-getter major-out minor-out patch-out)
    (list (pointer-u8-ref major-out)
          (pointer-u8-ref minor-out)
          (pointer-u8-ref patch-out))))

