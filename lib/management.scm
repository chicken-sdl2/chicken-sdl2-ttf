;;
;; chicken-sdl2-ttf: CHICKEN Scheme bindings to SDL_ttf 2
;;
;; Copyright © 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export open-font
        open-font*
        open-font-rw
        open-font-rw*
        close-font!)


(define-function-binding TTF_OpenFontIndex
  return: (TTF_Font* font-or-null)
  args: ((c-string file)
         (int ptsize)
         (long index)))

(define-function-binding TTF_OpenFontIndexRW
  return: (TTF_Font* font-or-null)
  args: ((SDL_RWops* src)
         (bool freesrc)
         (int ptsize)
         (long index)))

(define-function-binding TTF_CloseFont
  args: ((TTF_Font* font)))


(define-inline (%nonnull-font? x)
  (and (font? x) (not (struct-null? x))))


(: open-font
   (string fixnum #!optional fixnum -> ttf:font))
(define (open-font file ptsize #!optional (index 0))
  (set-finalizer! (open-font* file ptsize index)
                  close-font!))

(: open-font*
   (string fixnum #!optional fixnum -> ttf:font))
(define (open-font* file ptsize #!optional (index 0))
  (let ((font (TTF_OpenFontIndex file ptsize index)))
    (if (%nonnull-font? font)
        font
        (abort (sdl-failure "TTF_OpenFontIndex" #f)))))


(: open-font-rw
   (sdl2:rwops boolean fixnum #!optional fixnum -> ttf:font))
(define (open-font-rw rwops close? ptsize #!optional (index 0))
  (set-finalizer! (open-font-rw* rwops close? ptsize index)
                  close-font!))

(: open-font-rw*
   (sdl2:rwops boolean fixnum #!optional fixnum -> ttf:font))
(define (open-font-rw* rwops close? ptsize #!optional (index 0))
  (let ((font (TTF_OpenFontIndexRW rwops #f ptsize index)))
    (when close?
      ;; Properly close and nullify the sdl2:rwops.
      (rw-close! rwops))
    (if (%nonnull-font? font)
        font
        (abort (sdl-failure "TTF_OpenFontIndexRW" #f)))))


(: close-font!
   (ttf:font -> void))
(define (close-font! record)
  (unless (struct-null? record)
    (let ((ptr (font-pointer record)))
      (font-pointer-set! record (address->pointer 0))
      (unless (locative? ptr)
        (TTF_CloseFont ptr)))))
