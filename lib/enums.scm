;;
;; chicken-sdl2-ttf: CHICKEN Scheme bindings to SDL_ttf 2
;;
;; Copyright © 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export UNICODE_BOM_NATIVE
        UNICODE_BOM_SWAPPED)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UNICODE BYTE ORDER

(define-foreign-constants Uint16
  UNICODE_BOM_NATIVE
  UNICODE_BOM_SWAPPED)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; HINTING

(define-foreign-constants Uint16
  TTF_HINTING_NORMAL
  TTF_HINTING_LIGHT
  TTF_HINTING_MONO
  TTF_HINTING_NONE)


(define (hinting->symbol value #!optional not-found-callback)
  (select value
    ((TTF_HINTING_NORMAL) 'normal)
    ((TTF_HINTING_LIGHT)  'light)
    ((TTF_HINTING_MONO)   'mono)
    ((TTF_HINTING_NONE)   'none)
    (else (if not-found-callback
              (not-found-callback value)
              (error "invalid TTF hinting value" value)))))


(define (symbol->hinting symbol #!optional not-found-callback)
  (case symbol
    ((normal) TTF_HINTING_NORMAL)
    ((light)  TTF_HINTING_LIGHT)
    ((mono)   TTF_HINTING_MONO)
    ((none)   TTF_HINTING_NONE)
    (else (if not-found-callback
              (not-found-callback symbol)
              (error "invalid TTF hinting symbol" symbol)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; STYLE

(define-foreign-constants Uint16
  TTF_STYLE_NORMAL
  TTF_STYLE_BOLD
  TTF_STYLE_ITALIC
  TTF_STYLE_UNDERLINE
  TTF_STYLE_STRIKETHROUGH)


(define (style->symbol value #!optional not-found-callback)
  (select value
    ((TTF_STYLE_NORMAL)        'normal)
    ((TTF_STYLE_BOLD)          'bold)
    ((TTF_STYLE_ITALIC)        'italic)
    ((TTF_STYLE_UNDERLINE)     'underline)
    ((TTF_STYLE_STRIKETHROUGH) 'strikethrough)
    (else (if not-found-callback
              (not-found-callback value)
              (error "invalid TTF style value" value)))))


(define (symbol->style symbol #!optional not-found-callback)
  (case symbol
    ((normal)        TTF_STYLE_NORMAL)
    ((bold)          TTF_STYLE_BOLD)
    ((italic)        TTF_STYLE_ITALIC)
    ((underline)     TTF_STYLE_UNDERLINE)
    ((strikethrough) TTF_STYLE_STRIKETHROUGH)
    (else (if not-found-callback
              (not-found-callback symbol)
              (error "invalid TTF style symbol" symbol)))))


(define (pack-style syms #!optional not-found-callback)
  (cond
   ((integer? syms) syms)
   (else (apply bitwise-ior
           (map (lambda (sym)
                  (symbol->style sym not-found-callback))
                syms)))))


(define (unpack-style bitfield #!optional (exact? #f))
  (define bitmasks (list TTF_STYLE_BOLD
                         TTF_STYLE_ITALIC
                         TTF_STYLE_UNDERLINE
                         TTF_STYLE_STRIKETHROUGH))
  (map style->symbol
       (%separate-bitfield
        exact? bitfield bitmasks)))
