;;
;; chicken-sdl2-ttf: CHICKEN Scheme bindings to SDL_ttf 2
;;
;; Copyright © 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;; Helper macros and procedures. Mostly copied from chicken-sdl2.


;;; Attempt to translate the given glyph object into a Uint16 value.
;;;
;;; - Integers are returned as-is.
;;; - Characters are converted using char->integer.
;;;   (I don't know whether this works correctly for unicode chars.)
;;; - An error is signalled for any other type.
;;;
;;; More types might be supported in the future.
;;;
(define (%glyph->Uint16 glyph fn-name)
  (cond
   ((integer? glyph) glyph)
   ((char? glyph)    (char->integer glyph))
   (else (error fn-name "Unsupported glyph type:" glyph))))


;;; Copied from chicken-sdl2, except it does not export.
(define-syntax define-foreign-constants
  (syntax-rules ()
    ((define-foreign-constants foreign-type
       constant-name ...)
     (begin
       (define constant-name (foreign-value constant-name foreign-type))
       ...))))


;;; Copied from chicken-sdl2.
(define-syntax define-function-binding
  (syntax-rules (return: args:)
    ;; return type and args
    ((define-function-binding func-name-symbol
       return: (return-type return-semantics)
       args: ((arg-type arg-name) ...))
     (define func-name-symbol
       (foreign-lambda return-type func-name-symbol arg-type ...)))
    ;; no args
    ((define-function-binding func-name-symbol
       return: (return-type return-semantics))
     (define func-name-symbol
       (foreign-lambda return-type func-name-symbol)))
    ;; no return type (i.e. void)
    ((define-function-binding func-name-symbol
       args: ((arg-type arg-name) ...))
     (define func-name-symbol
       (foreign-lambda void func-name-symbol arg-type ...)))
    ;; no return type or args
    ((define-function-binding func-name-symbol)
     (define func-name-symbol
       (foreign-lambda void func-name-symbol)))))


;;; Copied from chicken-sdl2.
(define-syntax define-function-binding*
  (syntax-rules (return: args: body:)
    ;; return type and args
    ((define-function-binding* func-name-symbol
       return: (return-type return-semantics)
       args: ((arg-type arg-name) ...)
       body: body-string)
     (define func-name-symbol
       (foreign-lambda* return-type ((arg-type arg-name) ...) body-string)))
    ;; no args
    ((define-function-binding* func-name-symbol
       return: (return-type return-semantics)
       body: body-string)
     (define func-name-symbol
       (foreign-lambda* return-type () body-string)))
    ;; no return type (i.e. void)
    ((define-function-binding* func-name-symbol
       args: ((arg-type arg-name) ...)
       body: body-string)
     (define func-name-symbol
       (foreign-lambda* void ((arg-type arg-name) ...) body-string)))
    ;; no return type or args
    ((define-function-binding* func-name-symbol
       body: body-string)
     (define func-name-symbol
       (foreign-lambda* void () body-string)))))


(define-inline (%nonnull-surface? surf)
  (and (surface? surf) (not (struct-null? surf))))


;;; Copied from chicken-sdl2.
(define (%separate-bitfield exact? bitfield bitmasks)
  (filter (if exact?
              (lambda (bitmask)
                (= bitmask (bitwise-and bitfield bitmask)))
              (lambda (bitmask)
                (not (zero? (bitwise-and bitfield bitmask)))))
          bitmasks))


;;; Copied from chicken-sdl2.
(define-syntax with-temp-mem
  (syntax-rules ()
    ((with-temp-mem ((temp-var alloc-expr) ...)
       body ...)
     (let ((temp-var alloc-expr) ...)
       (receive result-values (begin body ...)
         (free temp-var)
         ...
         (apply values result-values))))))
