;;
;; chicken-sdl2-ttf: CHICKEN Scheme bindings to SDL_ttf 2
;;
;; Copyright © 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export glyph-provided
        glyph-metrics)


(define-function-binding TTF_GlyphIsProvided
  return: (int index-or-zero)
  args: ((TTF_Font* font)
         (Uint16 ch)))


(define-function-binding TTF_GlyphMetrics
  return: (int zero-on-success)
  args: ((TTF_Font* font)
         (Uint16 ch)
         (int* min-x-out)
         (int* max-x-out)
         (int* min-y-out)
         (int* max-y-out)
         (int* advance-out)))



(: glyph-provided
   (ttf:font (or fixnum char) -> (or fixnum boolean)))
(define (glyph-provided font glyph)
  (let* ((glyph-int (%glyph->Uint16 glyph 'glyph-provided))
         (index (TTF_GlyphIsProvided font glyph-int)))
    (if (zero? index)
        #f
        index)))


(: glyph-metrics
   (ttf:font (or fixnum char) -> fixnum fixnum fixnum fixnum fixnum))
(define (glyph-metrics font glyph)
  (with-temp-mem ((min-x-out   (%allocate-int))
                  (max-x-out   (%allocate-int))
                  (min-y-out   (%allocate-int))
                  (max-y-out   (%allocate-int))
                  (advance-out (%allocate-int)))
    (let* ((glyph-int (%glyph->Uint16 glyph 'glyph-metrics))
           (ret-code (TTF_GlyphMetrics font glyph-int
                                       min-x-out
                                       max-x-out
                                       min-y-out
                                       max-y-out
                                       advance-out)))
      (if (zero? ret-code)
          (values (pointer-s32-ref min-x-out)
                  (pointer-s32-ref max-x-out)
                  (pointer-s32-ref min-y-out)
                  (pointer-s32-ref max-y-out)
                  (pointer-s32-ref advance-out))
          (begin
            (free min-x-out)
            (free max-x-out)
            (free min-y-out)
            (free max-y-out)
            (free advance-out)
            (abort (sdl-failure "TTF_GlyphMetrics" ret-code)))))))
