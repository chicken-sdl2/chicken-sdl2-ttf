;;
;; chicken-sdl2-ttf: CHICKEN Scheme bindings to SDL_ttf 2
;;
;; Copyright © 2015, 2019  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(module sdl2-ttf ()

  (import (except scheme load))
  (cond-expand
    (chicken-4
     (import chicken foreign lolevel)
     (use srfi-1)
     (require-library sdl2 sdl2-internals))
    (chicken-5
     (import (chicken base)
             (chicken bitwise)
             (chicken condition)
             (chicken foreign)
             (chicken format)
             (chicken gc)
             (chicken locative)
             (chicken memory)
             (chicken module)
             (chicken type)
             (srfi 1)
             (only miscmacros select))))

  (import (only sdl2-internals
                struct-null?
                %allocate-int
                %allocate-Uint8
                sdl-failure
                color?    unwrap-color    wrap-color
                rwops?    unwrap-rwops    wrap-rwops
                surface?  unwrap-surface  wrap-surface)
          (only sdl2
                free-surface!
                rw-close!))


  (cond-expand
    (sdl2-ttf-use-mac-framework
     (foreign-declare "#include <SDL2_ttf/SDL_ttf.h>"))
    (else
     (foreign-declare "#include \"SDL_ttf.h\"")))

  (define-foreign-type Uint8*  (c-pointer unsigned-byte))
  (define-foreign-type Uint16  unsigned-short)
  (define-foreign-type Uint16* (c-pointer unsigned-short))
  (define-foreign-type Uint32  unsigned-integer32)
  (define-foreign-type int*    (c-pointer int))

  (define-foreign-type SDL_Color*
    (nonnull-c-pointer "SDL_Color")
    unwrap-color
    wrap-color)

  (define-foreign-type SDL_RWops*
    (nonnull-c-pointer "SDL_RWops")
    unwrap-rwops
    wrap-rwops)

  (define-foreign-type SDL_Surface*
    (nonnull-c-pointer "SDL_Surface")
    unwrap-surface
    wrap-surface)

  (define-type sdl2:color   (struct sdl2:color))
  (define-type sdl2:rwops   (struct sdl2:rwops))
  (define-type sdl2:surface (struct sdl2:surface))


  (include "lib/helpers.scm")
  (include "lib/font-type.scm")
  (include "lib/enums.scm")
  (include "lib/general.scm")
  (include "lib/management.scm")
  (include "lib/attributes.scm")
  (include "lib/glyph.scm")
  (include "lib/render.scm")

  ) ;; end module sdl2-ttf
