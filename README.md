# chicken-sdl2-ttf

[CHICKEN Scheme](http://call-cc.org/) bindings to
[SDL_ttf 2](http://www.libsdl.org/projects/SDL_ttf/),
a.k.a. the sdl2-ttf egg.

- Version:     0.2.0 (2019-02-13)
- Project:     https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf
- Issues:      https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf/issues
- Docs:        http://wiki.call-cc.org/eggref/5/sdl2-ttf
- License:     [BSD 2-Clause](LICENSE-BSD.txt)
- Maintainer:  John Croisant (john+chicken at croisant dot net)


## Synopsis

chicken-sdl2-ttf provides bindings to SDL_ttf version 2, the TrueType
Font rendering library. chicken-sdl2-ttf is built to be compatible
with [chicken-sdl2](https://gitlab.com/chicken-sdl2/chicken-sdl2)
(a.k.a. the sdl2 egg), which provides bindings to Simple DirectMedia
Layer version 2 (SDL2), a popular library used in games and other
media-rich software.


## Installation

If you run into trouble installing chicken-sdl2-ttf, please
[file a support request on the chicken-sdl2 project](https://gitlab.com/chicken-sdl2/chicken-sdl2/blob/master/CONTRIBUTING.md#filing-support-requests)
so we can help you, and so we can improve the install process and
instructions for future users.

### Dependencies

- [chicken-sdl2](https://gitlab.com/chicken-sdl2/chicken-sdl2),
  a.k.a. the sdl2 egg
- [SDL](http://libsdl.org/) 2.0 or higher
- [SDL_ttf](http://www.libsdl.org/projects/SDL_ttf/) 2.0 or higher

chicken-sdl2-ttf requires CHICKEN Scheme 4.8 or higher.
As of version 0.2.0, it is compatible with both CHICKEN 4 and CHICKEN 5.

### Installing the egg

In most cases, you can install the sdl2-ttf egg by running this command:

```
chicken-install sdl2-ttf
```

The installer will try to automatically determine the SDL2 compiler and linker flags
using the `sdl2-config` command.

In special cases, you may need to set the
`SDL2_CFLAGS`, `SDL2_LDFLAGS`, `SDL2_TTF_CFLAGS`, and/or `SDL2_TTF_LDFLAGS`
environment variables to provide the compiler and linker flags, then try again.
(The `SDL2_TTF_*` flags are used in addition to the `SDL2_*` flags.)

For example:

```
export SDL2_CFLAGS="-I/usr/local/include/SDL2"
export SDL2_LDFLAGS="-L/usr/local/lib -lSDL2"
export SDL2_TTF_LDFLAGS="-lSDL2_ttf"
chicken-install sdl2-ttf
```

These environment variables are needed only when installing the egg itself.
They are not needed when compiling or running games that use the egg.

### Installing on macOS

On macOS, the sdl2 egg can be compiled using either
UNIX-style libraries (e.g. compiled from source or installed via Homebrew)
or Mac-style frameworks (e.g. downloaded from the SDL website).

When automatically determining compiler and linker flags on macOS,
the installer will first check to see if `sdl2-config` is available.
If it is available, the installer will use it to determine the flags.

If `sdl2-config` is not available, the installer will check to see if
`SDL2.framework` and `SDL2_ttf.framework` are available
in either the `/Library/Frameworks` or `/System/Library/Frameworks` directories.
If they are available, the installer will use the frameworks.

If the frameworks are installed in some other directory,
or if you want to force using the framework even when `sdl2-config` is available,
set the `SDL2_CFLAGS`, `SDL2_LDFLAGS`, `SDL2_TTF_CFLAGS`, and/or `SDL2_TTF_LDFLAGS`
enviroment variables to tell the compiler where to find the frameworks:

```
export SDL2_CFLAGS="-F/path/to/your/frameworks"
export SDL2_LDFLAGS="-F/path/to/your/frameworks -framework SDL2"
export SDL2_TTF_LDFLAGS="-framework SDL2_ttf"
chicken-install sdl2
```

### Installing from git

If you want to try out the cutting edge, not-yet-released version of this library,
you can clone the git repository and install, like so:

```
git clone https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf.git
cd chicken-sdl2-ttf
chicken-install
```


## Demos and Examples

After you have installed chicken-sdl2-ttf, you can try compiling and running the demos and examples.

The [https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf/tree/master/demos](demos directory)
contains small programs demonstrating how to use various features of sdl2-ttf.

The [chicken-sdl2-examples repository](https://gitlab.com/chicken-sdl2/chicken-sdl2-examples)
contains complete example games and programs made with chicken-sdl2,
chicken-sdl2-ttf, and other related libraries.


## Contributing

chicken-sdl2-ttf is part of the
[chicken-sdl2 project](https://gitlab.com/chicken-sdl2/chicken-sdl2)

chicken-sdl2 is a volunteer effort, and your help is appreciated.
There are many ways to get involved in the project, whether you are an
experienced programmer or not. For more information about how you can
help, please see the chicken-sdl2
[contribution guide](https://gitlab.com/chicken-sdl2/chicken-sdl2/blob/master/CONTRIBUTING.md).

Please be aware that all project participants are expected to abide by
the [Contributor Code of Conduct](https://gitlab.com/chicken-sdl2/chicken-sdl2/blob/master/CODE_OF_CONDUCT.md).
We are committed to making participation in this project a welcoming
and harassment-free experience.
