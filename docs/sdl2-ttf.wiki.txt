
This file contains an archive of the sdl2-ttf egg wiki page.
The most up-to-date version of these docs is available at:
http://wiki.call-cc.org/eggref/5/sdl2-ttf


== sdl2-ttf

The sdl2-ttf egg provides bindings to
[[http://www.libsdl.org/projects/SDL_ttf/|SDL_ttf]] version 2.
SDL_ttf is a library for rendering text using TTF, OTF, and FON fonts.
It is compatible with [[http://libsdl.org/|Simple DirectMedia Layer]] version 2 (SDL2),
a popular library used in games and other software.

The sdl2-ttf egg is designed to be compatible with the
[[/egg/sdl2|sdl2]] egg, which provides bindings to SDL 2.

; Project / Source Code Repository : [[https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf]]
; Issue Tracker : [[https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf/issues]]
; Maintainer : John Croisant (john+chicken at croisant dot net)
; License: [[https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf/blob/master/LICENSE-BSD.txt|BSD 2-Clause]]


'''Table of Contents:'''

[[toc:]]


== Requirements

The sdl2-ttf egg requires the [[/egg/sdl2|sdl2]] egg,
[[http://libsdl.org/|Simple DirectMedia Layer]] 2.0.0 or higher,
and [[http://www.libsdl.org/projects/SDL_ttf/|SDL_ttf]]  2.0 or higher.
It will not work with older versions of SDL or SDL_ttf.

This egg requires CHICKEN Scheme 4.8 or higher.
As of version 0.2.0, this egg is compatible with both CHICKEN 4 and CHICKEN 5.


== Installation

In most cases, you can install the sdl2-ttf egg in the usual way:

 chicken-install sdl2-ttf

The installer will try to automatically determine the SDL2 compiler and linker flags
using the {{sdl2-config}} command.

In special cases, you may need to set the
{{SDL2_CFLAGS}}, {{SDL2_LDFLAGS}}, {{SDL2_TTF_CFLAGS}}, and/or {{SDL2_TTF_LDFLAGS}}
environment variables to provide the compiler and linker flags, then try again.
(The {{SDL2_TTF_*}} flags are used in addition to the {{SDL2_*}} flags.)

For example:

 export SDL2_CFLAGS="-I/usr/local/include/SDL2"
 export SDL2_LDFLAGS="-L/usr/local/lib -lSDL2"
 export SDL2_TTF_LDFLAGS="-lSDL2_ttf"
 chicken-install sdl2-ttf

These environment variables are needed only when installing the egg itself.
They are not needed when compiling or running programs that use the egg.

=== Installing on macOS

On macOS, the sdl2-ttf egg can be compiled using either
UNIX-style libraries (e.g. compiled from source or installed via Homebrew)
or Mac-style frameworks (e.g. downloaded from the SDL website).

When automatically determining compiler and linker flags on macOS,
the installer will first check to see if {{sdl2-config}} is available.
If it is available, the installer will use it to determine the flags.

If {{sdl2-config}} is not available, the installer will check to see if
{{SDL2.framework}} and {{SDL2_ttf.framework}} are available
in either the {{/Library/Frameworks}} or {{/System/Library/Frameworks}} directories.
If so, the installer will use the frameworks.

If the frameworks are installed in some other directory,
or if you want to force using the framework even when {{sdl2-config}} is available,
set the {{SDL2_CFLAGS}}, {{SDL2_LDFLAGS}}, {{SDL2_TTF_CFLAGS}}, and/or {{SDL2_TTF_LDFLAGS}}
enviroment variables to tell the compiler where to find the frameworks:

 export SDL2_CFLAGS="-F/path/to/your/frameworks"
 export SDL2_LDFLAGS="-F/path/to/your/frameworks -framework SDL2"
 export SDL2_TTF_LDFLAGS="-framework SDL2_ttf"
 chicken-install sdl2-ttf

== Usage and Examples

To avoid procedure name collisions, it is recommended that you import
the sdl2-ttf module using a prefix such as "ttf:", like so:

<enscript highlight="scheme">
(cond-expand
  (chicken-4 (use (prefix sdl2 "sdl2:")
                  (prefix sdl2-ttf "ttf:")))
  (chicken-5 (import (prefix sdl2 "sdl2:")
                     (prefix sdl2-ttf "ttf:"))))

(sdl2:set-main-ready!)
(sdl2:init!)
(ttf:init!)

(define font (ttf:open-font "ComicNeue-Regular.otf" 40))
(define text "Hello, World!")
(define-values (w h) (ttf:size-utf8 font text))
(define window (sdl2:create-window! text 'centered 'centered w h))

(let ((text-surf (ttf:render-utf8-shaded
                  font text
                  (sdl2:make-color 0   0   0)
                  (sdl2:make-color 255 255 255))))
  (sdl2:blit-surface! text-surf #f
                      (sdl2:window-surface window) #f))

(sdl2:update-window-surface! window)
(sdl2:delay! 5000)
(sdl2:quit!)
</enscript>

The [[https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf/tree/master/demos|demos directory]]
contains small programs demonstrating how to use various features of sdl2-ttf.

The [[https://gitlab.com/chicken-sdl2/chicken-sdl2-examples|chicken-sdl2-examples repository]]
contains complete example games and programs, some of which may use the sdl2-ttf egg.


== Version History

; 0.2.0 (2019-02-13) : Ported to be compatible with both CHICKEN 4 and CHICKEN 5. More user-friendly installation process.
; 0.1.0 (2015-12-25) : Initial release.

For more information about what changed in each version,
see the [[https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf/blob/master/CHANGELOG.md|CHANGELOG]].


== API

=== About the API

==== API Stability

The sdl2-ttf egg follows "[[http://semver.org/|semantic versioning]]".
Until version 1.0.0 is released, the API is not guaranteed to be "stable".
That means the maintainer reserves the right to change the API if needed,
possibly in ways that break backwards compatibility with previous versions.
'''Large backwards-incompatible changes are unlikely''',
but there may be small tweaks and fixes to the API if problems are discovered.

After version 1.0.0 is released, the API is guaranteed to remain stable (no backwards-incompatible changes)
until the next new major version (e.g. going from version 1.x to 2.0.0, or 2.x to 3.0.0).

==== Conventions

* Procedures names, including function bindings, are Scheme style.
** All procedure names are lower case and hyphenated, with no "TTF_" prefix.
** Procedures that ask a "true or false" question (aka predicates) are suffixed with "?".
   Usually words such as "has" or "is" are removed from predicate names.
** Procedures that cause a mutation or side effect are suffixed with "!".
** Procedures that set the value of a field are named like {{TYPE-FIELD-set!}}, e.g. {{font-style-set!}}.
   For setters that have a corresponding getter, it is also possible to use {{(set! ...)}} to set the value, e.g. {{(set! (font-style my-font) '(bold))}}.
   Both forms are equivalent.

* Some procedures signal an exception of kind {{(exn sdl2)}} if an error occurs or the operation fails.
  This is instead of returning an integer error code or null pointer.

* Some procedures have two versions: one that returns a managed object, and one that returns an unmanaged object.
  See [[/egg/sdl2#struct-memory-management|Struct Memory Management]] in the sdl2 egg docs.

* Some procedures return multiple values.
  For example, {{size-text}} returns two values, the width and height.
  You can use {{receive}}, {{let-values}}, etc. to capture all the return values.


=== General

<procedure>(init!)</procedure>

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_8.html#SEC8|TTF_Init]].

Signals an exception of kind {{(exn sdl2)}} if initialization fails.


<procedure>(was-init?) → boolean</procedure>

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_9.html#SEC9|TTF_WasInit]].


<procedure>(quit!)</procedure>

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html#SEC10|TTF_Quit]].


<procedure>(compiled-version) → list of integers</procedure>
<procedure>(current-version)  → list of integers</procedure>

Returns a list of three nonnegative integers, indicating a version number of SDL_ttf.
For example, the list {{(2 0 0)}} indicates SDL_ttf 2.0.0.

* {{compiled-version}} returns the version of SDL_ttf that the sdl2-ttf egg was compiled with.
* {{current-version}} returns the version of SDL_ttf that the sdl2-ttf egg is currently using.

For example, the user may have compiled the sdl2-ttf egg with SDL_ttf 2.0.0, then later upgraded SDL_ttf to 2.1.0, but not yet recompiled the sdl2-ttf egg with the new version.
In such a case, {{compiled-version}} would return {{(2 0 0)}}, and {{current-version}} would return {{(2 1 0)}}.
But, features from the new version would not be available until the user recompiles the sdl2-ttf egg.

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC7|SDL_TTF_VERSION and TTF_Linked_Version]].



=== Font

==== Font Loading

<procedure>(open-font  filepath ptsize #!optional index) → ttf:font</procedure>
<procedure>(open-font* filepath ptsize #!optional index) → ttf:font</procedure>

Attempts to load the TTF or FON font file at the given filepath (a string).
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_16.html#SEC16|TTF_OpenFontIndex]].

{{ptsize}} is the point size (based on 72DPI) to load the font as. This basically translates to pixel height.

{{index}} allows you to choose a font face from a file containing multiple font faces. It defaults to 0, the first face.

Returns a ttf:font with the font data.
Signals an exception of kind {{(exn sdl2)}} if the font could not be loaded.

* {{open-font}} returns a managed ttf:font.
* {{open-font*}} returns an unmanaged ttf:font, which must be closed with {{close-font!}} when you are done with it.


<procedure>(open-font-rw  rwops close? ptsize #!optional index) → ttf:font</procedure>
<procedure>(open-font-rw* rwops close? ptsize #!optional index) → ttf:font</procedure>

Attempts a TTF or FON font from an [[/egg/sdl2#rwops|sdl2:rwops]].
This procedure allows you to load a font from a variety of sources, such as a [[/man/4/Unit library#blobs|blob]], string, memory pointer, or file.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_17.html#SEC17|TTF_OpenFontIndexRW]].

If {{close?}} is #t, the sdl2:rwops will be automatically closed after the font is loaded.
See {{rw-close!}} in the sdl2 egg.
If {{close?}} is #f (the default), the sdl2:rwops will not be closed.

{{ptsize}} is the point size (based on 72DPI) to load the font as. This basically translates to pixel height.

{{index}} allows you to choose a font face from a file containing multiple font faces. It defaults to 0, the first face.

Returns a ttf:font with the font data.
Signals an exception of kind {{(exn sdl2)}} if the font could not be loaded.

* {{open-font-rw}} returns a managed ttf:font.
* {{open-font-rw*}} returns an unmanaged ttf:font, which must be closed with {{close-font!}} when you are done with it.


==== ttf:font

ttf:font is a record type that wraps a pointer to a
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_56.html#SEC56|TTF_Font]] struct.


<procedure>(font? obj) → boolean</procedure>

Returns #t if {{obj}} is a ttf:font.


<procedure>(close-font! font)</procedure>

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_18.html#SEC18|TTF_CloseFont]].

Close and free the memory of the ttf:font's underlying struct.
{{font}}'s pointer will be set to null (see {{struct-null?}} in the sdl2 egg).
It is safe to call this procedure with managed or unmanaged ttf:fonts.
It is safe (but has no effect) to free a struct record multiple times.


==== Font Attributes

<procedure>(font-style font) → list of symbols</procedure>
<setter>(set! (font-style font) style)</setter>
<setter>(font-style-set! font style)</setter>

Get or set the ttf:font's style, as a list of zero or more of the following symbols:

* {{bold}}
* {{italic}}
* {{underline}}
* {{strikethrough}}

Returns an empty list if the font has no style.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_21.html#SEC21|TTF_FontGetStyle]]
and [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_22.html#SEC22|TTF_FontSetStyle]].

The setters accept either a list of symbols or an integer,
representing bitwise-or'd integer constants.

'''NOTE:''' Bold and italic styles perform automatic adjustment of the font.
The results sometimes do not look very good.
For better-looking results, you should instead load the bold or italic versions of your font as a separate ttf:font.


<procedure>(font-outline font) → integer</procedure>
<setter>(set! (font-outline font) outline)</setter>
<setter>(font-outline-set! font outline)</setter>

Get or set the ttf:font's outline, as a nonnegative integer number of pixels.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_23.html#SEC23|TTF_GetFontOutline]]
and [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_24.html#SEC24|TTF_SetFontOutline]].


<procedure>(font-hinting font) → symbol</procedure>
<setter>(set! (font-hinting font) hinting)</setter>
<setter>(font-hinting-set! font hinting)</setter>

Get or set the ttf:font's hinting, as one of the following symbols:

* {{normal}}
* {{light}}
* {{mono}}
* {{none}}

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_25.html#SEC25|TTF_GetFontHinting]]
and [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_26.html#SEC26|TTF_SetFontHinting]].


<procedure>(font-kerning? font) → boolean</procedure>
<setter>(set! (font-kerning? font) kerning)</setter>
<setter>(font-kerning-set! font kerning)</setter>

Get or set the ttf:font's kerning, as a boolean.
#t means kerning is enabled, #f means kerning is disabled.

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_27.html#SEC27|TTF_GetFontKerning]]
and [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_28.html#SEC28|TTF_SetFontKerning]].


==== Font Metrics

<procedure>(font-height font) → integer</procedure>
<procedure>(font-ascent font) → integer</procedure>
<procedure>(font-descent font) → integer</procedure>
<procedure>(font-line-skip font) → integer</procedure>

Return various measurements of the ttf:font.

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_29.html#SEC29|TTF_FontHeight]],
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_30.html#SEC30|TTF_FontAscent]],
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_31.html#SEC31|TTF_FontDescent]],
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_32.html#SEC32|TTF_FontLineSkip]].


==== Font Faces

<procedure>(font-faces font) → integer</procedure>

Returns the number of faces ("sub-fonts") available in the ttf:font.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_33.html#SEC33|TTF_FontFaces]].


<procedure>(font-face-fixed-width? font) → boolean</procedure>

Returns #t if the current face of the given ttf:font is a fixed width font (i.e. every glyph is the same width).
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_34.html#SEC34|TTF_FontFaceIsFixedWidth]].


<procedure>(font-face-family-name font) → string</procedure>

Returns the current font face family name for the given ttf:font.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_35.html#SEC35|TTF_FontFaceFamilyName]].


<procedure>(font-face-style-name font) → string</procedure>

Returns the current font face style name for the given ttf:font.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_36.html#SEC36|TTF_FontFaceStyleName]].


==== Glyph Metrics

<procedure>(glyph-provided font glyph) → integer or #f</procedure>

If the glyph is provided by the font, this procedure returns a positive integer,
indicating the glyph's index position in the font.
If the glyph is not provided by the font, this procedure returns #f.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_37.html#SEC37|TTF_GlyphIsProvided]].

{{glyph}} may be an integer (representing a 16-bit Unicode character) or a Scheme character in the ASCII range.
Results may be incorrect if given a Scheme character outside the ASCII range.


<procedure>(glyph-metrics font glyph) → [min-x max-x min-y max-y advance]</procedure>

Return various metrics about the given glyph.
This procedure returns multiple values:

; min-x   : the glyph's minimum X offset
; max-x   : the glyph's maximum X offset
; min-y   : the glyph's minimum Y offset
; max-y   : the glyph's maximum Y offset
; advance : the glyph's advance offset

See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_38.html#SEC38|TTF_GlyphMetrics]].
There is a diagram which shows what these metrics mean.

{{glyph}} may be an integer (representing a 16-bit Unicode character) or a Scheme character in the ASCII range.
Results may be incorrect if given a Scheme character outside the ASCII range.

Signals an exception of kind {{(exn sdl2)}} if an error occurs.



=== Rendering

==== Rendering Latin-1 (ISO 8859-1) Text

<procedure>(size-text font text) → [w h]</procedure>

Calculate the size of the surface that ''would be created'' if you rendered the given Latin-1 (ISO 8859-1) text using the ttf:font.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_39.html#SEC39|TTF_SizeText]].

Returns two values, the calculated width and the height, as integers.

This is much faster than actually rendering the text,
so you can use this to quickly predict how much space would be needed.
This is useful for calculating word wrapping, alignment, truncation, etc.


<procedure>(render-text-solid  font text fg) → sdl2:surface</procedure>
<procedure>(render-text-solid* font text fg) → sdl2:surface</procedure>

Render the given Latin-1 (ISO 8859-1) encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"solid" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_43.html#SEC43|TTF_RenderText_Solid]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{text}} must be a Latin-1 (ISO 8859-1) encoded string specifying the text to render.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-text-solid}} returns a managed sdl2:surface.
* {{render-text-solid*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-text-shaded  font text fg bg) → sdl2:surface</procedure>
<procedure>(render-text-shaded* font text fg bg) → sdl2:surface</procedure>

Render the given Latin-1-encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"shaded" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_47.html#SEC47|TTF_RenderText_Shaded]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{text}} must be a Latin-1 (ISO 8859-1) encoded string specifying the text to render.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.
* {{bg}} must be a sdl2:color specifying the background color.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-text-shaded}} returns a managed sdl2:surface.
* {{render-text-shaded*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-text-blended  font text fg) → sdl2:surface</procedure>
<procedure>(render-text-blended* font text fg) → sdl2:surface</procedure>

Render the given Latin-1 (ISO 8859-1) encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"blended" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_51.html#SEC51|TTF_RenderText_Blended]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{text}} must be a Latin-1 (ISO 8859-1) encoded string specifying the text to render.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-text-blended}} returns a managed sdl2:surface.
* {{render-text-blended*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.



==== Rendering UTF8 Text

<procedure>(size-utf8 font text) → [w h]</procedure>

Calculate the size of the surface that ''would be created'' if you rendered the given UTF8 text using the ttf:font.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_40.html#SEC40|TTF_SizeUTF8]].

Returns two values, the calculated width and the height, as integers.

This is much faster than actually rendering the text,
so you can use this to quickly predict how much space would be needed.
This is useful for calculating word wrapping, alignment, truncation, etc.


<procedure>(render-utf8-solid  font text fg) → sdl2:surface</procedure>
<procedure>(render-utf8-solid* font text fg) → sdl2:surface</procedure>

Render the given UTF8 encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"solid" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_44.html#SEC44|TTF_RenderUTF8_Solid]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{text}} must be a UTF8 encoded string specifying the text to render.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-utf8-solid}} returns a managed sdl2:surface.
* {{render-utf8-solid*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-utf8-shaded  font text fg bg) → sdl2:surface</procedure>
<procedure>(render-utf8-shaded* font text fg bg) → sdl2:surface</procedure>

Render the given UTF8 encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"shaded" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_48.html#SEC48|TTF_RenderUTF8_Shaded]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{text}} must be a UTF8 encoded string specifying the text to render.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.
* {{bg}} must be a sdl2:color specifying the background color.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-utf8-shaded}} returns a managed sdl2:surface.
* {{render-utf8-shaded*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-utf8-blended  font text fg) → sdl2:surface</procedure>
<procedure>(render-utf8-blended* font text fg) → sdl2:surface</procedure>

Render the given UTF8 encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"blended" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_52.html#SEC52|TTF_RenderUTF8_Blended]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{text}} must be a UTF8 encoded string specifying the text to render.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-utf8-blended}} returns a managed sdl2:surface.
* {{render-utf8-blended*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.



==== Rendering Unicode Text

<procedure>(byte-swapped-unicode-set! swapped?)</procedure>
<constant>UNICODE_BOM_NATIVE</constant>
<constant>UNICODE_BOM_SWAPPED</constant>

{{byte-swapped-unicode-set!}} sets the default byte order of Unicode chars.
If {{swapped?}} is #t, this sets the default byte order to swapped.
If {{swapped?}} is #f, this sets the default byte order to native.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_20.html#SEC20|TTF_ByteSwappedUNICODE]].

UNICODE_BOM_NATIVE and UNICODE_BOM_SWAPPED are special Unicode characters (unsigned 16-bit integers).
When used in a Unicode string, they temporarily (for the remainder of the string)
override whether the byte order of Unicode chars is native or swapped.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_57.html#SEC57|UNICODE_BOM_NATIVE and UNICODE_BOM_SWAPPED]].


<procedure>(size-unicode font unicode) → [w h]</procedure>

Calculate the size of the surface that ''would be created'' if you rendered the given 16-bit Unicode encoded text using the ttf:font.
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_41.html#SEC41|TTF_SizeUNICODE]].

{{unicode}} must be a pointer or locative to a null-terminated C array of 16-bit unsigned integers, representing Unicode text. ([[/manual/Unit srfi-4|SRFI-4]] u16vectors can be wrapped in a locative using [[/manual/Unit lolevel#make-locative|make-locative]].)

Returns two values, the calculated width and the height, as integers.

This is much faster than actually rendering the text,
so you can use this to quickly predict how much space would be needed.
This is useful for calculating word wrapping, alignment, truncation, etc.


<procedure>(render-unicode-solid  font unicode fg) → sdl2:surface</procedure>
<procedure>(render-unicode-solid* font unicode fg) → sdl2:surface</procedure>

Render the given 16-bit Unicode encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"solid" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_45.html#SEC45|TTF_RenderUNICODE_Solid]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{unicode}} must be a pointer or locative to a null-terminated C array of 16-bit unsigned integers, representing Unicode text. ([[/manual/Unit srfi-4|SRFI-4]] u16vectors can be wrapped in a locative using [[/manual/Unit lolevel#make-locative|make-locative]].)
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-unicode-solid}} returns a managed sdl2:surface.
* {{render-unicode-solid*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-unicode-shaded  font unicode fg bg) → sdl2:surface</procedure>
<procedure>(render-unicode-shaded* font unicode fg bg) → sdl2:surface</procedure>

Render the given 16-bit Unicode encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"shaded" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_49.html#SEC49|TTF_RenderUNICODE_Shaded]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{unicode}} must be a pointer or locative to a null-terminated C array of 16-bit unsigned integers, representing Unicode text. ([[/manual/Unit srfi-4|SRFI-4]] u16vectors can be wrapped in a locative using [[/manual/Unit lolevel#make-locative|make-locative]].)
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.
* {{bg}} must be a sdl2:color specifying the background color.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-unicode-shaded}} returns a managed sdl2:surface.
* {{render-unicode-shaded*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-unicode-blended  font unicode fg) → sdl2:surface</procedure>
<procedure>(render-unicode-blended* font unicode fg) → sdl2:surface</procedure>

Render the given 16-bit Unicode encoded text using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"blended" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_53.html#SEC53|TTF_RenderUNICODE_Blended]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{unicode}} must be a pointer or locative to a null-terminated C array of 16-bit unsigned integers, representing Unicode text. ([[/manual/Unit srfi-4|SRFI-4]] u16vectors can be wrapped in a locative using [[/manual/Unit lolevel#make-locative|make-locative]].)
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the text itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered text.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-unicode-blended}} returns a managed sdl2:surface.
* {{render-unicode-blended*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.



==== Rendering Individual Glyphs

<procedure>(render-glyph-solid  font glyph fg) → sdl2:surface</procedure>
<procedure>(render-glyph-solid* font glyph fg) → sdl2:surface</procedure>

Render the given glyph using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"solid" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_46.html#SEC46|TTF_RenderGlyph_Solid]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{glyph}} must be an integer (representing a 16-bit Unicode character) or a Scheme character in the ASCII range.
  Results may be incorrect if given a Scheme character outside the ASCII range.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the glyph itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered glyph.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-glyph-solid}} returns a managed sdl2:surface.
* {{render-glyph-solid*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-glyph-shaded  font glyph fg bg) → sdl2:surface</procedure>
<procedure>(render-glyph-shaded* font glyph fg bg) → sdl2:surface</procedure>

Render the given glyph using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"shaded" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_50.html#SEC50|TTF_RenderGlyph_Shaded]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{glyph}} must be an integer (representing a 16-bit Unicode character) or a Scheme character in the ASCII range.
  Results may be incorrect if given a Scheme character outside the ASCII range.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the glyph itself.
* {{bg}} must be a sdl2:color specifying the background color.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered glyph.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-glyph-shaded}} returns a managed sdl2:surface.
* {{render-glyph-shaded*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.


<procedure>(render-glyph-blended  font glyph fg) → sdl2:surface</procedure>
<procedure>(render-glyph-blended* font glyph fg) → sdl2:surface</procedure>

Render the given glyph using
[[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_42.html#SEC42|"blended" rendering mode.]]
See [[http://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_54.html#SEC54|TTF_RenderGlyph_Blended]].

* {{font}} must be a ttf:font specifying the font to render with.
* {{glyph}} must be an integer (representing a 16-bit Unicode character) or a Scheme character in the ASCII range.
  Results may be incorrect if given a Scheme character outside the ASCII range.
* {{fg}} must be a sdl2:color specifying the foreground color, i.e. the color of the glyph itself.

Returns a [[/egg/sdl2#sdl2surface|sdl2:surface]] containing the rendered glyph.
Signals an exception of kind {{(exn sdl2)}} if an error occurred.

* {{render-glyph-blended}} returns a managed sdl2:surface.
* {{render-glyph-blended*}} returns an unmanaged sdl2:surface, which must be freed with {{free-surface!}} (from the sdl2 egg) when you are done with it.
